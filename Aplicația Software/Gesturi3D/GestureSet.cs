﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Gestures3D
{
    [XmlRoot("GestureSet")]
    [Serializable]
    [XmlInclude(typeof(Gesture))]
    public class GestureSet : List<Gesture>
    {
        public GestureSet()
        {
        }



        public static GestureSet ReadFromFile(string fileName)
        {
            if (fileName == null || !File.Exists(fileName))
                return new GestureSet();

            // XML files
            if (new FileInfo(fileName).Extension.ToUpper().CompareTo(".XML") == 0)
            {
                using (TextReader reader = new StreamReader(fileName))
                {
                    GestureSet gestureSet = (GestureSet)new XmlSerializer(typeof(GestureSet)).Deserialize(reader);
                    foreach (Gesture gesture in gestureSet)
                        gesture.ProcessGesture();
                    return gestureSet;
                }
            }
            else
                if (new FileInfo(fileName).Extension.ToUpper().CompareTo("JSON") == 0) // JSON files
            {
                // ..
            }

            return new GestureSet(); ;
        }

        public void WriteToFile(string fileName)
        {
            using (TextWriter writer = new StreamWriter(fileName))
                new XmlSerializer(typeof(GestureSet)).Serialize(writer, this);
        }

    }
}
