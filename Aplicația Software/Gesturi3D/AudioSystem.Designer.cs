﻿namespace Gestures3D
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbCanvas = new System.Windows.Forms.PictureBox();
            this.tbGestureName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkAddToTrainingSet = new System.Windows.Forms.CheckBox();
            this.btnFM = new System.Windows.Forms.Button();
            this.btnCD = new System.Windows.Forms.Button();
            this.btnAux = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.progressVolume = new System.Windows.Forms.ProgressBar();
            this.txbDisplay = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbCanvas)).BeginInit();
            this.SuspendLayout();
            // 
            // pbCanvas
            // 
            this.pbCanvas.Location = new System.Drawing.Point(279, 202);
            this.pbCanvas.Margin = new System.Windows.Forms.Padding(4);
            this.pbCanvas.Name = "pbCanvas";
            this.pbCanvas.Size = new System.Drawing.Size(325, 200);
            this.pbCanvas.TabIndex = 0;
            this.pbCanvas.TabStop = false;
            this.pbCanvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbCanvas_MouseDown);
            this.pbCanvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbCanvas_MouseMove);
            this.pbCanvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbCanvas_MouseUp);
            // 
            // tbGestureName
            // 
            this.tbGestureName.Location = new System.Drawing.Point(112, 435);
            this.tbGestureName.Margin = new System.Windows.Forms.Padding(4);
            this.tbGestureName.Name = "tbGestureName";
            this.tbGestureName.Size = new System.Drawing.Size(480, 22);
            this.tbGestureName.TabIndex = 1;
            this.tbGestureName.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 435);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nume gest";
            this.label1.Visible = false;
            // 
            // chkAddToTrainingSet
            // 
            this.chkAddToTrainingSet.AutoSize = true;
            this.chkAddToTrainingSet.Location = new System.Drawing.Point(29, 465);
            this.chkAddToTrainingSet.Margin = new System.Windows.Forms.Padding(4);
            this.chkAddToTrainingSet.Name = "chkAddToTrainingSet";
            this.chkAddToTrainingSet.Size = new System.Drawing.Size(244, 21);
            this.chkAddToTrainingSet.TabIndex = 3;
            this.chkAddToTrainingSet.Text = "adauga gest la setul de antrenare";
            this.chkAddToTrainingSet.UseVisualStyleBackColor = true;
            this.chkAddToTrainingSet.Visible = false;
            // 
            // btnFM
            // 
            this.btnFM.Location = new System.Drawing.Point(684, 271);
            this.btnFM.Name = "btnFM";
            this.btnFM.Size = new System.Drawing.Size(75, 23);
            this.btnFM.TabIndex = 4;
            this.btnFM.Text = "FM";
            this.btnFM.UseVisualStyleBackColor = true;
            // 
            // btnCD
            // 
            this.btnCD.Location = new System.Drawing.Point(684, 300);
            this.btnCD.Name = "btnCD";
            this.btnCD.Size = new System.Drawing.Size(75, 23);
            this.btnCD.TabIndex = 5;
            this.btnCD.Text = "CD";
            this.btnCD.UseVisualStyleBackColor = true;
            // 
            // btnAux
            // 
            this.btnAux.Location = new System.Drawing.Point(684, 329);
            this.btnAux.Name = "btnAux";
            this.btnAux.Size = new System.Drawing.Size(75, 23);
            this.btnAux.TabIndex = 6;
            this.btnAux.Text = "AUX";
            this.btnAux.UseVisualStyleBackColor = true;
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(765, 358);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(49, 23);
            this.btn1.TabIndex = 7;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(820, 358);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(49, 23);
            this.btn2.TabIndex = 8;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            // 
            // btn3
            // 
            this.btn3.Location = new System.Drawing.Point(875, 358);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(49, 23);
            this.btn3.TabIndex = 9;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            // 
            // btn4
            // 
            this.btn4.Location = new System.Drawing.Point(930, 358);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(49, 23);
            this.btn4.TabIndex = 10;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            // 
            // btn5
            // 
            this.btn5.Location = new System.Drawing.Point(985, 358);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(49, 23);
            this.btn5.TabIndex = 11;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            // 
            // progressVolume
            // 
            this.progressVolume.Location = new System.Drawing.Point(765, 313);
            this.progressVolume.Name = "progressVolume";
            this.progressVolume.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.progressVolume.Size = new System.Drawing.Size(269, 23);
            this.progressVolume.TabIndex = 12;
            this.progressVolume.Value = 50;
            // 
            // txbDisplay
            // 
            this.txbDisplay.Location = new System.Drawing.Point(765, 285);
            this.txbDisplay.Name = "txbDisplay";
            this.txbDisplay.ReadOnly = true;
            this.txbDisplay.Size = new System.Drawing.Size(269, 22);
            this.txbDisplay.TabIndex = 13;
            this.txbDisplay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(327, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "Enter a gesture to change you\'re car audio system";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Gesturi3D.Properties.Resources.images;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1160, 490);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txbDisplay);
            this.Controls.Add(this.progressVolume);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.btnAux);
            this.Controls.Add(this.btnCD);
            this.Controls.Add(this.btnFM);
            this.Controls.Add(this.chkAddToTrainingSet);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbGestureName);
            this.Controls.Add(this.pbCanvas);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormMain";
            this.Text = "BMW Profesional Audio System";
            ((System.ComponentModel.ISupportInitialize)(this.pbCanvas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbCanvas;
        private System.Windows.Forms.TextBox tbGestureName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkAddToTrainingSet;
        private System.Windows.Forms.Button btnFM;
        private System.Windows.Forms.Button btnCD;
        private System.Windows.Forms.Button btnAux;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.ProgressBar progressVolume;
        private System.Windows.Forms.TextBox txbDisplay;
        private System.Windows.Forms.Label label2;
    }
}

