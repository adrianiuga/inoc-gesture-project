using System;
using System.Xml.Serialization;

namespace Gestures3D
{
    /// <summary>
    /// Implements a gesture as a cloud of points (i.e., an unordered set of points).
    /// Gestures are normalized with respect to scale, translated to origin, and resampled into a fixed number of 32 points.
    /// </summary>
    [Serializable]
    public class Gesture
    {
        [XmlElement("Point")]
        public Point[] RawPoints = null;         // gesture points (raw)

        [XmlIgnore]
        public Point[] Points = null;            // gesture points (normalized)

        [XmlAttribute("Name")]
        public string Name = "";                 // gesture class
        
        /// <summary>
        /// Constructs an empty gesture
        /// </summary>
        public Gesture()
        {
            Name = "";
            RawPoints = new Point[0];
            Points = new Point[0];
        }

        /// <summary>
        /// Constructs a gesture from an array of points
        /// </summary>
        /// <param name="points"></param>
        public Gesture(Point[] points, string gestureName = "")
        {
            this.Name = gestureName;

            // make a copy of the raw points
            this.RawPoints = new Point[points.Length];
            Array.Copy(points, RawPoints, points.Length);
            ProcessGesture();
        }

        /// <summary>
        /// Normalizes the array of points with respect to scale, origin, and number of points
        /// </summary>
        public void ProcessGesture()
        {
            this.Points = Scale(this.RawPoints);
            this.Points = TranslateTo(Points, Centroid(Points));
            this.Points = Resample(Points, 64);
        }

        #region gesture pre-processing steps: scale normalization, translation to origin, and resampling

        /// <summary>
        /// Performs scale normalization with shape preservation into [0..1]x[0..1]
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        private Point[] Scale(Point[] points)
        {
            double minx = double.MaxValue, miny = double.MaxValue, maxx = double.MinValue, maxy = double.MinValue,
                minz = double.MaxValue, maxz = double.MinValue;
            for (int i = 0; i < points.Length; i++)
            {
                if (minx > points[i].X) minx = points[i].X;
                if (miny > points[i].Y) miny = points[i].Y;
                if (minz > points[i].Z) minz = points[i].Z;
                if (maxx < points[i].X) maxx = points[i].X;
                if (maxy < points[i].Y) maxy = points[i].Y;
                if (maxz < points[i].Z) maxz = points[i].Z;
            }

            Point[] newPoints = new Point[points.Length];
            double scale = Math.Max(Math.Max(maxx - minx, maxy - miny), maxz - minz);
            for (int i = 0; i < points.Length; i++)
                newPoints[i] = new Point(
                    (points[i].X - minx) / scale,
                    (points[i].Y - miny) / scale,
                    (points[i].Z - minz) / scale,
                    points[i].T, points[i].StrokeID
                );
            return newPoints;
        }

        /// <summary>
        /// Translates the array of points by p
        /// </summary>
        /// <param name="points"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        private Point[] TranslateTo(Point[] points, Point p)
        {
            Point[] newPoints = new Point[points.Length];
            for (int i = 0; i < points.Length; i++)
                newPoints[i] = new Point(points[i].X - p.X, points[i].Y - p.Y, points[i].Z - p.Z, points[i].T, points[i].StrokeID);
            return newPoints;
        }

        /// <summary>
        /// Computes the centroid for an array of points
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        private Point Centroid(Point[] points)
        {
            double cx = 0, cy = 0, cz = 0;
            for (int i = 0; i < points.Length; i++)
            {
                cx += points[i].X;
                cy += points[i].Y;
                cz += points[i].Z;
            }
            return new Point(cx / points.Length, cy / points.Length, cz / points.Length, 0, 0);
        }

        /// <summary>
        /// Resamples the array of points into n equally-distanced points
        /// </summary>
        /// <param name="points"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public Point[] Resample(Point[] points, int n)
        {
            Point[] newPoints = new Point[n];
            newPoints[0] = new Point(points[0].X, points[0].Y, points[0].Z, points[0].T, points[0].StrokeID);
            int numPoints = 1;

            double I = PathLength(points) / (n - 1); // computes interval length
            double D = 0;
            for (int i = 1; i < points.Length; i++)
            {
                if (points[i].StrokeID == points[i - 1].StrokeID)
                {
                    double d = Geometry.EuclideanDistance(points[i - 1], points[i]);
                    if (D + d >= I)
                    {
                        Point firstPoint = points[i - 1];
                        while (D + d >= I)
                        {
                            // add interpolated point
                            double t = Math.Min(Math.Max((I - D) / d, 0.0f), 1.0f);
                            if (double.IsNaN(t)) t = 0.5f;
                            newPoints[numPoints++] = new Point(
                                (1.0f - t) * firstPoint.X + t * points[i].X,
                                (1.0f - t) * firstPoint.Y + t * points[i].Y,
                                (1.0f - t) * firstPoint.Z + t * points[i].Z,
                                (long)((1.0f - t) * firstPoint.T + t * points[i].T),
                                points[i].StrokeID
                            );

                            // update partial length
                            d = D + d - I;
                            D = 0;
                            firstPoint = newPoints[numPoints - 1];
                        }
                        D = d;
                    }
                    else D += d;
                }
            }

            if (numPoints == n - 1) // sometimes we fall a rounding-error short of adding the last point, so add it if so
                newPoints[numPoints++] = new Point(points[points.Length - 1].X, points[points.Length - 1].Y, points[points.Length - 1].Z, points[points.Length - 1].T, points[points.Length - 1].StrokeID);
            return newPoints;
        }

        /// <summary>
        /// Computes the path length for an array of points
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        private double PathLength(Point[] points)
        {
            double length = 0;
            for (int i = 1; i < points.Length; i++)
                if (points[i].StrokeID == points[i - 1].StrokeID)
                    length += Geometry.EuclideanDistance(points[i - 1], points[i]);
            return length;
        }

        #endregion
    }
}