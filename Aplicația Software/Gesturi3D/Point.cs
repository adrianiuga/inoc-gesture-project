using System;
using System.Xml.Serialization;

namespace Gestures3D
{
    /// <summary>
    /// Implements a 2D Point that exposes X, Y, and StrokeID properties.
    /// StrokeID is the stroke index the point belongs to (e.g., 0, 1, 2, ...) that is filled by counting pen down/up events.
    /// </summary>
    public class Point
    {
        [XmlAttribute("X")]
        public double X;

        [XmlAttribute("Y")]
        public double Y;

        [XmlAttribute("Z")]
        public double Z;

        [XmlAttribute("T")]
        public long T;

        [XmlAttribute("Stroke")]
        public int StrokeID;

        public Point()
        {
            this.X = 0;
            this.Y = 0;
            this.T = 0;
            this.StrokeID = 0;
        }

        public Point(double x, double y, double z, long t, int strokeId)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
            this.T = t;
            this.StrokeID = strokeId;
        }
    }
}
