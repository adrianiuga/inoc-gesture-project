﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using Google.Apis.Util;

namespace Gestures3D
{
    public partial class FormMain : Form
    {
        private List<Point> points = null;
        private GestureSet gestureSet = null;
        private const string TRAINING_FILE_NAME = "training.xml";

        private string currentSystemMode = string.Empty;
        private string currentTrackRemaining = TrackList.Track1;
        private string currentFMRemaining = FmList.Fm1;



        public FormMain()
        {
            InitializeComponent();
            pbCanvas.Image = new Bitmap(pbCanvas.Width, pbCanvas.Height);
            Draw();
            this.gestureSet = GestureSet.ReadFromFile(TRAINING_FILE_NAME);
            Console.WriteLine("{0} gestures loaded.", this.gestureSet.Count);
            foreach (Gesture g in gestureSet)
                Console.WriteLine("{0} / {1} points", g.Name, g.Points.Length);
        }

        private void Draw()
        {
            using (Graphics graphics = Graphics.FromImage(pbCanvas.Image))
            {
                graphics.Clear(Color.White);
                int R = 5;
                if (points != null)
                    foreach (Point point in points)
                        graphics.FillEllipse(Brushes.Black, (int)(point.X - R), (int)(point.Y - R), 2 * R, 2 * R);
            }
            pbCanvas.Refresh();
        }

        #region gesture acquisition

        private bool isDrawing = false;

        private void pbCanvas_MouseDown(object sender, MouseEventArgs e)
        {
            isDrawing = true;
            points = new List<Point>();
        }

        private void pbCanvas_MouseUp(object sender, MouseEventArgs e)
        {
            isDrawing = false;

            // construct gesture from a list of 2-D points
            Gesture gesture = new Gesture(points.ToArray(), tbGestureName.Text.Trim());

            // classify gesture
            //MessageBox.Show(PointCloudRecognizer.Classify(gesture, gestureSet.ToArray()));
            classifyBMWGesture(gesture);

            // add gesture to the training set?
            if (chkAddToTrainingSet.Checked)
            {
                gestureSet.Add(gesture);
                gestureSet.WriteToFile(TRAINING_FILE_NAME);
            }
        }

        private void pbCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDrawing)
            {
                points.Add(new Point(e.X, e.Y, 0, (long)DateTime.Now.TimeOfDay.TotalMilliseconds, 0));
                Draw();
            }
        }

        private void classifyBMWGesture(Gesture gesture)
        {
            var curentGesture = PointCloudRecognizer.Classify(gesture, gestureSet.ToArray());

            if (curentGesture == "aux" || curentGesture == "fm" || curentGesture == "fm2" || curentGesture == "fm3" || curentGesture == "fm4"
                || curentGesture == "cd" || curentGesture == "volume-up" || curentGesture == "volume-down")
            {
                currentSystemMode = curentGesture;

                if (currentSystemMode == "aux")
                {
                    txbDisplay.Text = " AUX ";
                }
                else if (currentSystemMode == "fm" || currentSystemMode == "fm2" || currentSystemMode == "fm3" || currentSystemMode == "fm4")
                {
                    txbDisplay.Text = currentFMRemaining;
                }
                else if (currentSystemMode == "cd")
                {
                    txbDisplay.Text = currentTrackRemaining;
                }
                else if (currentSystemMode == "volume-up")
                {
                    if (progressVolume.Value >= 0 && progressVolume.Value <= 75)
                    {
                        progressVolume.Value += 25;
                    }
                }
                else if (currentSystemMode == "volume-down")
                {
                    if (progressVolume.Value >= 25 && progressVolume.Value <= 100)
                    {
                        progressVolume.Value -= 25;
                    }
                }

            }
            else
            {
                if (currentSystemMode == "fm" || currentSystemMode == "fm2" || currentSystemMode == "fm3" || currentSystemMode == "fm4")
                {
                    if (curentGesture == "1")
                    {
                        currentFMRemaining = FmList.Fm1;
                        txbDisplay.Text = currentFMRemaining;
                    }
                    if (curentGesture == "2")
                    {
                        currentFMRemaining = FmList.Fm2;
                        txbDisplay.Text = currentFMRemaining;
                    }
                    if (curentGesture == "3" || curentGesture == "33")
                    {
                        currentFMRemaining = FmList.Fm3;
                        txbDisplay.Text = currentFMRemaining;
                    }
                    if (curentGesture == "4")
                    {
                        currentFMRemaining = FmList.Fm4;
                        txbDisplay.Text = currentFMRemaining;
                    }
                    if (curentGesture == "5")
                    {
                        currentFMRemaining = FmList.Fm5;
                        txbDisplay.Text = currentFMRemaining;
                    }
                }
                else if (currentSystemMode == "cd")
                {
                    if (curentGesture == "1")
                    {
                        currentTrackRemaining = TrackList.Track1;
                        txbDisplay.Text = currentTrackRemaining;
                    }
                    if (curentGesture == "2")
                    {
                        currentTrackRemaining = TrackList.Track2;
                        txbDisplay.Text = currentTrackRemaining;
                    }
                    if (curentGesture == "3" || curentGesture == "33")
                    {
                        currentTrackRemaining = TrackList.Track3;
                        txbDisplay.Text = currentTrackRemaining;
                    }
                    if (curentGesture == "4")
                    {
                        currentTrackRemaining = TrackList.Track4;
                        txbDisplay.Text = currentTrackRemaining;
                    }
                    if (curentGesture == "5")
                    {
                        currentTrackRemaining = TrackList.Track5;
                        txbDisplay.Text = currentTrackRemaining;
                    }
                }
            }

        }

        #endregion
    }


    public static class TrackList
    {
        public const string
            Track1 = "Track 1",
            Track2 = "Track 2",
            Track3 = "Track 3",
            Track4 = "Track 4",
            Track5 = "Track 5";
    }

    public static class FmList
    {
        public const string
            Fm1 = "94.2 FM",
            Fm2 = "102.7 FM",
            Fm3 = "102.2 FM",
            Fm4 = "100.4 FM",
            Fm5 = "105.3 FM";
    }
}
